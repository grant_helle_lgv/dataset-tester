import requests
import json
import csv
import os

#Global variable!
base_url = 'https://geodienste.hamburg.de/HH_OAF/datasets/'

#Requests datasets from OAF instance and converts to JSON
def makeRequest(base_url = base_url, proxies = {'http': '', 'https':''}):
    req = requests.get(base_url, proxies=proxies)
    request_data = req.json()
    return request_data

def getDatasetLength(request_data):
    dataset_length = len(request_data['datasets'])
    return dataset_length

#Returns python list of dataset names
def getDatasetNames(request_data):
    dataset_names = []
    dataset_length = getDatasetLength(request_data)
    for i in range(dataset_length):
        dataset_names.append(request_data['datasets'][i]['name'])
    return dataset_names

#Returns python list of collection JSON Objects
def getCollections(datasetName, base_url = base_url):
    collections = []
    for i in range(getDatasetLength(makeRequest())):
        collections_url = base_url + datasetName[i] + '/collections?f=json'
        collection = makeRequest(collections_url)
        collections.append(collections[i]['collections'])
        #print(i, '\n' + collections_url + '\n')
    return collections
'''
def getItems(collections):
    for i in range(len(collections)):
        print(collections[i]['collections'])
'''
###TESTS
request_data = makeRequest()
datasetNames = getDatasetNames(request_data)

collections = getCollections(datasetNames)
print(collections)
