import requests
import json
import csv
import os
from datetime import datetime

print('Script started')
#Set file location path
file_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')

#OGC API for Features instance

base_url = 'https://geodienste.hamburg.de/HH_OAF/datasets/'
local_url = 'http://localhost:8081/deegree-services-oaf/datasets/'

#Run against local environment
base_url = local_url


#If proxy system environment variable is not already set, fill it in here
proxies = {
  'http': '',
  'https': '',
}

#Create list objects for errors
error_url = []
error_timeout = []

#Set timeout in seconds
timeout= 600

#Remove trailing / in base url
request_url = base_url[:-1]
req = requests.get(request_url, proxies=proxies, timeout=timeout)
data = req.json()

count = 0

#Get all dataset names
dataset_length = len(data['datasets'])
for i in range(dataset_length):
    ds_name = data['datasets'][i]['name']

    #Get collections
    collections_url = base_url + ds_name + '/collections?f=json'
    try: collections_response = requests.get(collections_url, proxies=proxies, timeout=timeout)
    except: 
        print("Timeout Error! \n" + collections_url + '\n')
        now = datetime.now()
        error_timeout.append(str(now) + ' ' + collections_url)
        continue

    collections = collections_response.json()
    count += 1
    print('\nTest ' + str(count) + '/' + str(dataset_length) + '\n' + collections_url + '\n')
    
    #Get items
    collections_num = len(collections['collections'])
    for i in range(collections_num):
        
        items_name = collections['collections'][i]['title']
        items_url = base_url + ds_name + '/collections/' + items_name + '/items?f=json'
        print('Testing items: ', items_name, '\n', items_url)
        try:
            items_response = requests.get(items_url, proxies=proxies, timeout=timeout)
            if items_response.status_code != 200:
                now = datetime.now()
                error_url.append(str(now) + ' ' + items_url)
                print('Page does not exist')
                continue

        except: 
            print("Timeout Error! \n" + items_url + '\n')
            now = datetime.now()
            error_timeout.append(str(now) + ' ' + items_url)
            continue

        items = items_response.json()

        try:
            if items_response.status_code != 200:
                now = datetime.now()
                error_url.append(str(now) + ' ' + items_url)

            elif items_response.status_code == 200:
                feature_name = items['features'][0]['id']
                feature_url = base_url + ds_name + '/collections/' + items_name + '/items/' + feature_name + '?f=json'
                try:
                    feature_response = requests.get(feature_url, proxies=proxies, timeout=timeout)
                    if feature_response.status_code != 200:
                        now = datetime.now()
                        error_url.append(str(now) + ' ' + feature_url)                      
                    else:
                        pass
                except ConnectionResetError as exception:
                    now = datetime.now() 
                    error_timeout.append(str(now) + ' ' + feature_url)
                    print(exception + feature_url)  
                    continue         
            else:
                pass
        except:
            continue

#Report errors and timeouts to terminal + file
print('These datasets contained errors: \n')
print(error_url) 
with open(os.path.join(file_path,'error_url.json'), 'w', encoding='utf-8') as f:
    json.dump(error_url,f, ensure_ascii=False, indent=4)


print('\n These datasets contained timeout errors: \n')
print(error_timeout)     
error_timeout_json = json.dumps(error_timeout)
with open(os.path.join(file_path,'error_timeout.json'), 'w', encoding='utf-8') as f:
    json.dump(error_timeout,f, ensure_ascii=False, indent=4)